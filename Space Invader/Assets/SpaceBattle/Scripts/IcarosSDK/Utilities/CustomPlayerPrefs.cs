﻿using System;
using UnityEngine;

namespace IcarosSDK.Utilities
{
    public sealed class CustomPlayerPrefs
    {
        private const int DEFAULT_INT_VALUE = 0;
    
        private const float DEFAULT_FLOAT_VALUE = 0f;
    
        private const int TRUE_INT_VALUE = 1;
    
        private const int FALSE_INT_VALUE = 0;
    
        private static readonly string DefaultStringValue = string.Empty;
    
        public static void SetBool(string key, bool value, bool saveImmediately = true)
        {
            int value2 = (!value) ? 0 : 1;
            CustomPlayerPrefs.SetInt(key, value2, saveImmediately);
        }
    
        public static void SetFloat(string key, float value, bool saveImmediately = true)
        {
            PlayerPrefs.SetFloat(key, value);
            if (saveImmediately)
            {
                CustomPlayerPrefs.Save();
            }
        }
    
        public static void SetInt(string key, int value, bool saveImmediately = true)
        {
            PlayerPrefs.SetInt(key, value);
            if (saveImmediately)
            {
                CustomPlayerPrefs.Save();
            }
        }
    
        public static void SetDateTime(string key, DateTime value, bool saveImmediately = true)
        {
            PlayerPrefs.SetString(key, value.ToBinary().ToString());
            if (saveImmediately)
            {
                CustomPlayerPrefs.Save();
            }
        }
    
        public static void SetString(string key, string value, bool saveImmediately = true)
        {
            PlayerPrefs.SetString(key, value);
            if (saveImmediately)
            {
                CustomPlayerPrefs.Save();
            }
        }
    
        public static void SetEnumValue<T>(string key, T value, bool saveImmediately = true) where T : struct, IConvertible
        {
            CustomPlayerPrefs.SetString(key, value.ToString(), saveImmediately);
        }
    
        public static bool GetBool(string key, bool defaultValue = false)
        {
            var v = CustomPlayerPrefs.GetInt(key, -1);
            if (v == -1)
                v = defaultValue ? 1 : 0;
            return v == 1;
        }
    
        public static float GetFloat(string key)
        {
            return CustomPlayerPrefs.GetFloat(key, 0f);
        }
    
        public static float GetFloat(string key, float defaultValue)
        {
            return PlayerPrefs.GetFloat(key, defaultValue);
        }
    
        public static int GetInt(string key)
        {
            return CustomPlayerPrefs.GetInt(key, 0);
        }
    
        public static int GetInt(string key, int defaultValue)
        {
            return PlayerPrefs.GetInt(key, defaultValue);
        }
    
        public static DateTime GetDateTime(string key, DateTime defaultValue)
        {
            string @string = PlayerPrefs.GetString(key);
            if (!string.IsNullOrEmpty(@string))
            {
                long dateData = Convert.ToInt64(@string);
                return DateTime.FromBinary(dateData);
            }
            return defaultValue;
        }
    
        public static string GetString(string key)
        {
            return CustomPlayerPrefs.GetString(key, CustomPlayerPrefs.DefaultStringValue);
        }
    
        public static string GetString(string key, string defaultValue)
        {
            return PlayerPrefs.GetString(key, defaultValue);
        }
    
        public static T GetEnumValue<T>(string key, T defaultValue) where T : struct, IConvertible
        {
            string @string = CustomPlayerPrefs.GetString(key);
            if (!string.IsNullOrEmpty(@string))
            {
                return (T)((object)Enum.Parse(typeof(T), @string));
            }
            return defaultValue;
        }
    
        public static bool HasKey(string key)
        {
            return PlayerPrefs.HasKey(key);
        }
    
        public static void DeleteKey(string key)
        {
            PlayerPrefs.DeleteKey(key);
            CustomPlayerPrefs.Save();
        }
    
        public static void DeleteAll()
        {
            PlayerPrefs.DeleteAll();
            CustomPlayerPrefs.Save();
        }
    
        public static void Save()
        {
            PlayerPrefs.Save();
        }
    }

}