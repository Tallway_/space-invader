﻿using IcarosSDK.Utilities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Icalethics.Calibration
{
    public static class CalibrationManager
    {
        public static Matrix4x4 calibrationMatrix;
        public static float currentCosine, currentSine;

        public static float startSine
        {
            get
            {
                return CustomPlayerPrefs.GetFloat("StartSine", 0);
            }

            set
            {
                CustomPlayerPrefs.SetFloat("StartSine", value);
            }
        }

        public static float startCosine
        {
            get
            {
                return CustomPlayerPrefs.GetFloat("StartCosine", 0);
            }

            set
            {
                CustomPlayerPrefs.SetFloat("StartCosine", value);
            }
        }

        private static bool isFirstTime = true;

        private static readonly int sizeFilter = 30;
        private static Vector3[] filter;
        private static Vector3 filterSum = Vector3.zero;
        private static int posFilter = 0;
        private static int qSamples = 0;

        private static bool isCalibratedAccelerometer = false;

        public static bool isWasCalibration;

        public static Vector3 gyroCalibrationRotation
        {
            get
            {
                float x = CustomPlayerPrefs.GetFloat("gyroCalibrationRotation_x", 0);
                float y = CustomPlayerPrefs.GetFloat("gyroCalibrationRotation_y", 0);
                float z = CustomPlayerPrefs.GetFloat("gyroCalibrationRotation_z", 0);

                return new Vector3(x, y, z);
            }

            set
            {
                CustomPlayerPrefs.SetFloat("gyroCalibrationRotation_x", value.x);
                CustomPlayerPrefs.SetFloat("gyroCalibrationRotation_y", value.y);
                CustomPlayerPrefs.SetFloat("gyroCalibrationRotation_z", value.z);
            }
        }

        public static void CalibrateAccelerometer()
        {
            // Mapping to our left hand coordinate system
            Quaternion rotateQuaternion = new Quaternion(-0.5f, -0.5f, 0.5f, -0.5f);
            rotateQuaternion *= Quaternion.Euler(0, 55, 0); // Fix flat device calibration
            // Creating the identity matrix
            Matrix4x4 matrix = Matrix4x4.TRS(Vector3.zero, rotateQuaternion, new Vector3(1f, 1f, 1f));
            // Getting the inverse of the matrix
            calibrationMatrix = matrix.inverse;

            isCalibratedAccelerometer = true;
        }

        public static void Recalibrate()
        {
            if (!isCalibratedAccelerometer)
                CalibrateAccelerometer();

            startSine = currentSine;
            startCosine = currentCosine;

            CalibrateGyro();

            isWasCalibration = true;
        }

        public static void CalibrateGyro()
        {
            gyroCalibrationRotation = GetGravityRotation();
        }

        public static Vector3 GetRawGravityRotation()
        {
            Input.gyro.enabled = true;

            Vector3 gyro = 90 * Input.gyro.gravity;

            if (gyroCalibrationRotation.y > 80 || gyroCalibrationRotation.y < -80)
            {
                return new Vector3(gyro.x, gyro.y, gyro.z);
            }
            else
            {
                return new Vector3(gyro.x, gyro.y, -1 * gyro.y / 1.085173502f);
            }
        }

        /*
         * Get data from the software sensor "gravity" 
         * (a combination of accelerometer and gyroscope)
         * which renders a smooth but responsive result,
         * better than the accelerometer with filters.
         * Returns the rotation of the device in degrees
         */
        public static Vector3 GetGravityRotation()
        {
            return GetRawGravityRotation();
        }

        public static Vector3 GetAccelerometer(Vector3 accelerator)
        {
            Vector3 dir = calibrationMatrix.MultiplyVector(accelerator);
            if (dir.sqrMagnitude > 1)
                dir.Normalize();
            return dir;
        }

        public static Vector3 MoveAverageFilter(Vector3 sample)
        {
            if (qSamples == 0)
                filter = new Vector3[sizeFilter];

            filterSum += sample - filter[posFilter];
            filter[posFilter++] = sample;

            if (posFilter > qSamples)
                qSamples = posFilter;
            posFilter = posFilter % sizeFilter;

            return filterSum / qSamples;
        }
    }
}
