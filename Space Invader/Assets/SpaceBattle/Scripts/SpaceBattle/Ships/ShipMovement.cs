﻿using System.Collections;
using System.Collections.Generic;
using SpaceBattle.Global;
using UnityEngine;

namespace SpaceBattle.Ships
{
    [RequireComponent(typeof(CircleCollider2D), typeof(Rigidbody2D))]
    public class ShipMovement : MonoBehaviour
    {
        [Space, SerializeField, Range(0,90)] private float _pitchSpeed;
        [SerializeField, Range(0,90)] private float _rollSpeed;
    
        private GameBounds _bounds;
        private Rigidbody2D _rigidbody;
        private CircleCollider2D _circleCollider;
        private Vector2 _accelerationNormarizedValue; 
        private SpaceBattleGravityCalculator _acceleration;
    
    
        private void Awake()
        {
            _rigidbody = GetComponent<Rigidbody2D>();
            _circleCollider = GetComponent<CircleCollider2D>();
            _acceleration = GetComponent<SpaceBattleGravityCalculator>();
            
            float colliderSize = _circleCollider.radius * transform.localScale.x;
    
            _bounds = new GameBounds(CameraInfo.CameraBounds.MinX + colliderSize,
                                        CameraInfo.CameraBounds.MaxX - colliderSize, 
                                        CameraInfo.CameraBounds.MinY + colliderSize,
                                        0f - colliderSize);
        }
    
        private void OnEnable() 
        {
            GameUpdater.Instance.OnUpdate += OnUpdate;
            GameUpdater.Instance.OnFixedUpdate += OnFixedUpdate;
        }
    
        private void OnDisable() 
        {
            GameUpdater.Instance.OnUpdate -= OnUpdate;
            GameUpdater.Instance.OnFixedUpdate -= OnFixedUpdate;
        }
    
        private void OnUpdate()
        {
            Vector3 clampedPosition = transform.position;
    
            clampedPosition.x = Mathf.Clamp(clampedPosition.x, _bounds.MinX, _bounds.MaxX);
            clampedPosition.y = Mathf.Clamp(clampedPosition.y, _bounds.MinY, _bounds.MaxY);
    
            transform.position = clampedPosition;
        }
    
        private void OnFixedUpdate()
        {
            _accelerationNormarizedValue = new Vector2(-_acceleration.RollNormalized, _acceleration.PitchNormalized);
    
            Vector2 playerVelocity = new Vector2(_accelerationNormarizedValue.x * _rollSpeed, _accelerationNormarizedValue.y * _pitchSpeed);
    
            _rigidbody.velocity = playerVelocity;
        }
    }
}
