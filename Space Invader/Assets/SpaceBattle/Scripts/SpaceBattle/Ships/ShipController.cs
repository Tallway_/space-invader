﻿using System;
using SpaceBattle.Buffs;
using SpaceBattle.Bullets;
using SpaceBattle.Enemies;
using SpaceBattle.Enums;
using SpaceBattle.Global;
using SpaceBattle.Interfaces;
using SpaceBattle.ObjectPoolers;
using UnityEngine;

namespace SpaceBattle.Ships
{
    [RequireComponent(typeof(SpaceBattleGravityCalculator), typeof(ShipMovement), typeof(ShipRotation))]
    public class ShipController : MonoBehaviour, IDamagable
    {
        [SerializeField] private TurretManager _turretManager;
        [SerializeField] private SocketManager _socketManager;
        [SerializeField] private int _shipHealth;

        public event Action OnDead;
        public event Action<BulletsBuff> OnBuffReceived;
        public event Action<TurretBuff> OnTurretBuffReceived;
        public event Action<int> OnHealthChanged;

        private Animator _animator;
        private int _health;

        public int Health 
        { 
            get 
            { 
                return _health; 
            } 

            set
            {
                if(value <= 0)
                {
                    _health = 0;
                    
                    OnHealthChanged?.Invoke(_health);

                    OnDead?.Invoke();

                    AdditionalPooler.Instance.GetObjectFromPool(AdditionalType.Explosion, transform.position, Quaternion.identity);

                    Destroy(gameObject);
                }
                else if(value > _shipHealth)
                {
                    _health = _shipHealth;
                }
                else 
                {
                    _health = value;
                }

                OnHealthChanged?.Invoke(_health);
            }
        }

        private void Awake()
        {
            _animator = GetComponentInChildren<Animator>();
       
            _health = _shipHealth;

            _turretManager.Init(this);
            _socketManager.Init(this);
        }

        private void OnEnable() 
        {
            GameManager.Instance.OnGameStarted += OnGameStarted;
            GameManager.Instance.OnGameOver += OnGameOver;
        }

        private void OnDisable() 
        {
            GameManager.Instance.OnGameStarted -= OnGameStarted;
            GameManager.Instance.OnGameOver -= OnGameOver;
        }

        private void OnGameOver()
        {
            _turretManager.DisactivateTurret();
        }

        private void OnGameStarted()
        {
            _turretManager.ActivateTurret();
        }

        private void OnTriggerEnter2D(Collider2D other) 
        {
            if(other.TryGetComponent(out Bullet bullet))
            {
                if(bullet.Owner == GameInstances.Enemy)
                {
                    ApplyDamage(bullet.DamageValue);
                }
            }

            if(other.TryGetComponent(out IDisactivatable someObject))
            {
                someObject.Disactivate();
            }
        }

        public void ApplyDamage(int value)
        {
            Health -= value;
            
            _animator.SetTrigger("ApplyDamage");
        }

        public void ReceiveBuff(BulletsBuff bulletBuff)
        {
            OnBuffReceived?.Invoke(bulletBuff);
        }

        public void ReceiveTurretBuff(TurretBuff turretBuff)
        {
            OnTurretBuffReceived?.Invoke(turretBuff);
        }
    }
}
