﻿using System.Collections.Generic;
using SpaceBattle.Enums;
using UnityEngine;

namespace SpaceBattle.Ships
{
    public class Socket : MonoBehaviour
    {
        public List<Transform> ListOfSlots;
        public BuffType BuffType;
        public Sprite SocketSpite;
    }
}
