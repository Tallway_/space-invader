﻿using System.Collections;
using System.Collections.Generic;
using SpaceBattle.Global;
using UnityEngine;

namespace SpaceBattle.Ships
{
    public class ShipRotation : MonoBehaviour
    {      
        [SerializeField, Range(0,10)] private float _reduceSpeedValue;

        private SpaceBattleGravityCalculator _acceleration;

        private float _xAccelerationValue;

        private void Awake() 
        {
            _acceleration = GetComponent<SpaceBattleGravityCalculator>();
        }

        private void OnEnable() 
        {
            GameUpdater.Instance.OnUpdate += OnUpdate;
        }

        private void OnDisable() 
        {
            GameUpdater.Instance.OnUpdate -= OnUpdate;
        }

        private void OnUpdate()
        {
            _xAccelerationValue = _acceleration.RollNormalized;

            transform.rotation = new Quaternion(0f, 0f, _xAccelerationValue / _reduceSpeedValue, 1f);
        }
    }
}
