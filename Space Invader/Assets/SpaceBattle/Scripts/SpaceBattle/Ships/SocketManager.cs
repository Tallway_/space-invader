﻿using System;
using System.Collections;
using System.Collections.Generic;
using SpaceBattle.Buffs;
using SpaceBattle.Enums;
using SpaceBattle.UI;
using UnityEngine;

namespace SpaceBattle.Ships
{
    public class SocketManager : MonoBehaviour
    {
        public event Action<float> OnTimeAdding;
        public event Action<Socket, float> OnSocketChanged;

        [SerializeField] private List<Socket> _sockets;
        [SerializeField, Space] private Socket _startSocket;

        private ShipController _ship;
        private Socket _currentSocket;

        public void Init(ShipController ship)
        {
            _ship = ship;
            _currentSocket = _startSocket;
        }

        private void OnEnable() 
        {
            BulletBuffTimer.OnBulletBuffTimerOver += OnTurretBuffTimerOver;
            _ship.OnBuffReceived += buff => OnBuffReceived(buff);
        }

        private void OnDisable() 
        {
            BulletBuffTimer.OnBulletBuffTimerOver -= OnTurretBuffTimerOver;
            _ship.OnBuffReceived -= OnBuffReceived;
        }

        private void OnTurretBuffTimerOver()
        {
            TurretManager.Socket = _startSocket;
        }

        private void OnBuffReceived(BulletsBuff bulletBuff)
        {
            if(_currentSocket.BuffType == bulletBuff.BuffsType)
            {
                OnTimeAdding?.Invoke(bulletBuff.UsingTime);
                return;
            }

            foreach(Socket socket in _sockets)
            {
                if(socket.BuffType == bulletBuff.BuffsType)
                {
                    TurretManager.Socket = socket;

                    OnSocketChanged?.Invoke(socket, bulletBuff.UsingTime);
                    break;
                }
            }
        }

        public Socket GetStartSocket()
        {
            return _startSocket;
        }
    }
}
