﻿using System;
using System.Collections.Generic;
using SpaceBattle.Buffs;
using SpaceBattle.Enums;
using SpaceBattle.Global;
using SpaceBattle.UI;
using SpaceBattle.Interfaces;
using UnityEngine;

namespace SpaceBattle.Ships
{
    public class TurretManager : MonoBehaviour
    {
        [SerializeField] private SocketManager _socketManager;
        [SerializeField] private List<GameObject> _turretObjects;

        public event Action<float> OnTimeAdding;
        public event Action<Sprite, float> OnTurretChanged;

        private ShipController _ship;
        private List<ITurret> _shipTurrets;
        private ITurret _turret, _startTurret;

        public static Socket Socket { get; set; }

        public void Init(ShipController ship)
        {
            _ship = ship;
            _shipTurrets = new List<ITurret>();
        }

        private void OnEnable() 
        {
            TurretBuffTimer.OnTurretBuffTimerOver += OnTurretBuffTimerOver;
            _ship.OnTurretBuffReceived += OnTurretBuffReceived;
        }

        private void OnDisable() 
        {
            TurretBuffTimer.OnTurretBuffTimerOver -= OnTurretBuffTimerOver;
            _ship.OnTurretBuffReceived -= OnTurretBuffReceived;
        }


        private void Start() 
        {
            foreach(GameObject turretObject in _turretObjects)
            {
                ITurret turret = turretObject.GetComponent<ITurret>();
                turret.Disactivate();

                _shipTurrets.Add(turret);
            }  

            _startTurret = _shipTurrets.Find(turret => turret.TurretType == TurretType.SimpleTurret); 
            _turret = _startTurret; 
            Socket = _socketManager.GetStartSocket();     
        }

        private void OnTurretBuffTimerOver()
        {
            _turret.Disactivate();

            _turret = _startTurret;

            _turret.Activate();
        }

        private void OnTurretBuffReceived(TurretBuff turretBuff)
        {         
            if(_turret.TurretType == turretBuff.TurretType)
            {
                OnTimeAdding?.Invoke(turretBuff.UsingTime);
                return;
            } 

            ChangeTurretTo(turretBuff);
        }

        private void ChangeTurretTo(TurretBuff turretBuff)
        {
            _turret.Disactivate();

            _turret = _shipTurrets.Find(turret => turret.TurretType == turretBuff.TurretType); 

            _turret.Activate();

            OnTurretChanged?.Invoke(turretBuff.BulletSprite, turretBuff.UsingTime);
        }

        public void ActivateTurret()
        {
            _turret.Activate();
        }

        public void DisactivateTurret()
        {
            _turret.Disactivate();
        }
    }
}
