﻿using SpaceBattle.Enums;

namespace SpaceBattle.Interfaces
{
    public interface IBuff
    {
        BuffType BuffsType { get; }
    }
}
