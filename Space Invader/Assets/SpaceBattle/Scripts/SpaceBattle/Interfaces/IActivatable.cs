﻿namespace SpaceBattle.Interfaces
{
    public interface IActivatable
    {
        void Activate();
    }
}