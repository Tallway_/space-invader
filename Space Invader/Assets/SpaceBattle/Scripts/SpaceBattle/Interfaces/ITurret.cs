﻿using SpaceBattle.Enums;
using UnityEngine;

namespace SpaceBattle.Interfaces
{
    public interface ITurret 
    {
        TurretType TurretType { get; }
        void Activate();
        void Disactivate();
    }
}
