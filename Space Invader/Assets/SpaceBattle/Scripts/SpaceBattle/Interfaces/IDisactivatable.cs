﻿namespace SpaceBattle.Interfaces
{
    public interface IDisactivatable
    {
        void Disactivate();
    }
}
