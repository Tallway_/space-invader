﻿namespace SpaceBattle.Interfaces
{
    public interface IDamagable 
    {
        void ApplyDamage(int value);
    }
}
