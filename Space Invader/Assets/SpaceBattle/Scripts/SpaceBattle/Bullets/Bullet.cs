﻿using SpaceBattle.Enums;
using SpaceBattle.Interfaces;
using SpaceBattle.ObjectPoolers;
using UnityEngine;

namespace SpaceBattle.Bullets
{
    public class Bullet : MonoBehaviour, IActivatable ,IDisactivatable
    {
        [SerializeField] private int _damageValue; 
        [SerializeField] private float _speed;
        [SerializeField] private BulletType _bulletType;

        public GameInstances Owner { get; private set; }
        public int DamageValue { get { return _damageValue; } }

        private Vector2 _directionOfVelocity;
        private Rigidbody2D _rigidbody;


        private void Awake() 
        {
            _rigidbody = GetComponent<Rigidbody2D>();
        }

        public void Init(GameInstances owner, int layer) 
        {
            Owner = owner;
            gameObject.layer = layer;
        }

        public void ReturnToPool()
        {
            BulletPooler.Instance.ReturnObjectToPool(this.gameObject, _bulletType);
        }

        public void Disactivate()
        {
            ReturnToPool();
        }

        public void Activate()
        {
            _rigidbody.velocity = transform.up * _speed;
        }
    }
}
