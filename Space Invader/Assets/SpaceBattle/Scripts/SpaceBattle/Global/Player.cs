﻿using System;
using SpaceBattle.Ships;
using UnityEngine;

namespace SpaceBattle.Global
{
    [DefaultExecutionOrder(-1)]
    public class Player : MonoBehaviour
    {
        public event Action<Player> OnPlayerShipDestroyed;
        public event Action<int> OnScoreChanged;

        [SerializeField] private ShipController Ship;
        
        public int Score 
        { 
            get
            {
                return _score;
            }
            
            set
            {
                _score = value;
                
                OnScoreChanged?.Invoke(value);
            }
        }

        private ShipController _ship;
        private int _score;

        private void Awake() 
        {
            _ship = Instantiate(Ship, transform.position, Quaternion.identity, transform);
            _ship.OnDead += OnDead;

            _score = 0;
        }

        private void OnDisable() 
        {
            _ship.OnDead -= OnDead;
        }

        private void OnDead()
        {
            OnPlayerShipDestroyed?.Invoke(this);
        }
    }
}
