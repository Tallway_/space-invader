﻿using System.Collections;
using System.Collections.Generic;
using SpaceBattle.Wave.States;
using UnityEngine;

namespace SpaceBattle.Global
{
    public abstract class State<T>
    {
        protected T _manager;
        protected StateMachine<T> _stateMachine;

        protected State(T manager, StateMachine<T> stateMachine)
        {
            _manager = manager;
            _stateMachine = stateMachine;
        }

        public virtual void Enter() { }

        public virtual void Update() { }

        public virtual void Exit() { }
    }
}
