﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceBattle.Global
{
    [DefaultExecutionOrder(-3)]
    public class GameUpdater : MonoBehaviour
    {
        public static GameUpdater Instance { get { return _instance; } }
        private static GameUpdater _instance;

        public event Action OnUpdate;
        public event Action OnFixedUpdate;
        public event Action OnLateUpdate;

        private void Awake() 
        {
            if(_instance != null && _instance != this)
            {
                Destroy(this.gameObject);
            }
            else
            {
                _instance = this;
            }
        }
        
        private void Update()
        {
            OnUpdate?.Invoke();
        }

        private void FixedUpdate() 
        {
            OnFixedUpdate?.Invoke();
        }

        private void LateUpdate() 
        {
            OnLateUpdate?.Invoke();
        }
    }
}
