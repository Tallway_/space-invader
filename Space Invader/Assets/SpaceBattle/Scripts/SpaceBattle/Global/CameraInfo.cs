﻿using UnityEngine;

namespace SpaceBattle.Global
{
    [DefaultExecutionOrder(-1)]
    public class CameraInfo : MonoBehaviour
    {
        public Camera MainCamera { get; private set;}
        public static GameBounds CameraBounds { get; private set; }

        private void Awake() 
        {
            MainCamera = Camera.main;

            Vector3 maxCameraValue = MainCamera.ViewportToWorldPoint(new Vector3(1, 1, MainCamera.nearClipPlane));
            Vector3 minCameraValue = MainCamera.ViewportToWorldPoint(new Vector3(0, 0, MainCamera.nearClipPlane));

            CameraBounds = new GameBounds(minCameraValue.x, maxCameraValue.x, minCameraValue.y, maxCameraValue.y);
        }
    }
}
