﻿using Icalethics.Calibration;
using UnityEngine;

namespace SpaceBattle.Global
{
    public class SpaceBattleGravityCalculator : MonoBehaviour
    {
        private Vector2 pitchRollAngleSpeed = Vector2.zero;
        private Vector2 midTilt = Vector2.zero;
        private Vector2 tiltXLimit = Vector2.zero;
        private Vector2 tiltZLimit = Vector2.zero;

        private bool isPitchBlock = false;
        private bool isRollBlock = false;

        private bool isInversePitch = false;
        private bool isInverseRoll = true;

        private float smoothnessDelay = 1.5f;
        private float smoothnessDelayByRotate = 1.5f;
        private float maxSpeed = 150f;
        private float currentSpeed = 0;

        private const float MAX_PITCH = 90;
        
        private const float MAX_ROLL = 35;

        private void UpdateSpeed()
        {
            pitchRollAngleSpeed.x = MAX_ROLL;
            pitchRollAngleSpeed.y = MAX_PITCH;

            tiltXLimit.x = midTilt.x - pitchRollAngleSpeed.x;
            tiltXLimit.y = midTilt.x + pitchRollAngleSpeed.x;
            tiltZLimit.x = midTilt.y - pitchRollAngleSpeed.y;
            tiltZLimit.y = midTilt.y + pitchRollAngleSpeed.y;
        }

        private void Start()
        {
            Time.timeScale = 1;

            midTilt.x = CalibrationManager.gyroCalibrationRotation.x;
            midTilt.y = CalibrationManager.gyroCalibrationRotation.z;

            UpdateSpeed();
        }

        protected Vector3 gravityRotation;

        public float Pitch
        {
            get { return gravityRotation.y; }
        }
        
        public float PitchNormalized
        {
            get
            {
                return gravityRotation.y / MAX_PITCH;
            }
        }
        
        public float Roll
        {
            get { return gravityRotation.x; }
        }
        
        public float RollNormalized
        {
            get { return gravityRotation.x / MAX_ROLL; }
        }

        private void FixedUpdate()
        {
            midTilt.x = CalibrationManager.gyroCalibrationRotation.x;
            midTilt.y = CalibrationManager.gyroCalibrationRotation.z;

            UpdateSpeed();

            gravityRotation = CalibrationManager.GetGravityRotation();

            gravityRotation.x = Mathf.Clamp(gravityRotation.x, tiltXLimit.x, tiltXLimit.y);
            gravityRotation.z = Mathf.Clamp(gravityRotation.z, tiltZLimit.x, tiltZLimit.y);

            gravityRotation.x = isInverseRoll ? -(gravityRotation.x - midTilt.x) : (gravityRotation.x - midTilt.x);
            gravityRotation.z = isInversePitch ? (gravityRotation.z - midTilt.y) : -(gravityRotation.z - midTilt.y);

            //Debug.LogWarning(new Vector2(RollNormalized, PitchNormalized));
        }
    }
}