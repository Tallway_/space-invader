﻿using UnityEngine;

namespace SpaceBattle.Global
{
    public class StateMachine<T>
    {
        public State<T> CurrentState { get; private set; }

        public void Initialize(State<T> startState)
        {
            CurrentState = startState;
        }

        public void ChangeState(State<T> newState)
        {
            CurrentState.Exit();

            CurrentState = newState;
            
            newState.Enter();
        }
    }
}
