﻿using SpaceBattle.Bullets;
using SpaceBattle.Enemies;
using SpaceBattle.Interfaces;
using UnityEngine;

namespace SpaceBattle.Global
{
    public class DeadZone : MonoBehaviour
    {
        private BoxCollider2D _deadZoneCollider;
        private GameBounds _deadZoneBounds;

        private void Awake() 
        {
            _deadZoneCollider = GetComponent<BoxCollider2D>();
            _deadZoneBounds = new GameBounds(CameraInfo.CameraBounds.MinX,
                                            CameraInfo.CameraBounds.MaxX,
                                            CameraInfo.CameraBounds.MinY,
                                            CameraInfo.CameraBounds.MaxY);

            float xColliderSize = _deadZoneBounds.MaxX * 2f + 2f;
            float yColliderSize = _deadZoneBounds.MaxY * 2f + 5f;

            _deadZoneCollider.size = new Vector2(xColliderSize, yColliderSize);
        }

        private void OnTriggerExit2D(Collider2D other) 
        {
            if(other.gameObject.TryGetComponent(out IDisactivatable someObject))
            {
                someObject.Disactivate();
            }
        }
    }
}
