﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using SpaceBattle.Wave;

namespace SpaceBattle.Global
{
    [DefaultExecutionOrder(-2)]
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private WaveManager _waveManager;

        public static GameManager Instance { get { return _instance; } }
        private static GameManager _instance;

        public event Action OnGameStarted;
        public event Action OnGameOver;
        public event Action OnBossKilled;
        public event Action<int> OnCountdownValueChanged;

        private int countdownValue = 3;
        private List<Player> _players;

        private void Awake() 
        {
            if(_instance != null && _instance != this)
            {
                Destroy(this.gameObject);
            }
            else
            {
                _instance = this;
            }

            _players = new List<Player>();
        }


        private void Start() 
        {
            _waveManager.GameEnding.OnWavesCompleted += OnWavesCompleted;
            _waveManager.BossSpawning.OnBossDead += OnBossDead;

            _players = FindObjectsOfType<Player>().ToList();

            foreach (Player player in _players)
            {
                player.OnPlayerShipDestroyed += OnPlayerShipDestroyed;
            }

            StartCoroutine(StartCountdown());
        }

        private void OnDisable() 
        {
            _waveManager.GameEnding.OnWavesCompleted -= OnWavesCompleted;
            _waveManager.BossSpawning.OnBossDead -= OnBossDead;
        }

        private void OnBossDead()
        {
            OnBossKilled?.Invoke();
        }

        private void OnWavesCompleted()
        {
            OnGameOver?.Invoke();
        }

        private void OnPlayerShipDestroyed(Player player)
        {
            player.OnPlayerShipDestroyed -= OnPlayerShipDestroyed;

            Player playerToRemove = _players.Find(somePlayer => somePlayer.gameObject.tag == player.tag);
            _players.Remove(playerToRemove);

            if(_players.Count == 0)
            {
                OnGameOver?.Invoke();
            }
        }

        public void RestartGame()
        {
            SceneManager.LoadScene(0);
        }

        public void QuitApplication()
        {
            Application.Quit();
        }

        private IEnumerator StartCountdown()
        {
            WaitForSeconds pause = new WaitForSeconds(1f);

            for(int i = countdownValue; i > 0; i--)
            {
                OnCountdownValueChanged?.Invoke(i);

                yield return pause;
            }

            OnGameStarted?.Invoke();
        }
    }
}
