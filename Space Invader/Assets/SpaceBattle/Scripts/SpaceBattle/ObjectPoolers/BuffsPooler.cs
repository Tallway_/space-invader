﻿using SpaceBattle.Enums;

namespace SpaceBattle.ObjectPoolers
{
    public class BuffsPooler : ObjectPooler<BuffType> 
    {
        public static BuffsPooler Instance { get { return _instance; } }
        private static BuffsPooler _instance;

        private void Awake() 
        {
            if(_instance != null && _instance != this)
            {
                Destroy(this.gameObject);
            }
            else
            {
                _instance = this;
            }

            Init();
        }
    }
}
