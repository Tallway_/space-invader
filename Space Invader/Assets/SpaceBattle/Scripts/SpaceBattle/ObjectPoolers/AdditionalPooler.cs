﻿using SpaceBattle.Enums;

namespace SpaceBattle.ObjectPoolers
{
    public class AdditionalPooler : ObjectPooler<AdditionalType>
    {
        public static AdditionalPooler Instance { get { return _instance; } }
        private static AdditionalPooler _instance;

        private void Awake() 
        {
            if(_instance != null && _instance != this)
            {
                Destroy(this.gameObject);
            }
            else
            {
                _instance = this;
            }

            Init();
        }
    }
}
