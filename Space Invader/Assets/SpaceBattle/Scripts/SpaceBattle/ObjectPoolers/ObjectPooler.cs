﻿using System.Collections.Generic;
using UnityEngine;

namespace SpaceBattle.ObjectPoolers
{
    [System.Serializable]
    public class Pool<T>
    {
        public T Type;
        public GameObject Prefab;
        public int ObjectsAmount;
    }

    public class ObjectPooler<T>: MonoBehaviour
    {
        [SerializeField] protected List<Pool<T>> _pools = new List<Pool<T>>();
        [SerializeField] bool _shouldExpand;
        protected Dictionary<T, List<GameObject>> _poolDictionary;
        
        protected void Init() 
        {
            _poolDictionary = new Dictionary<T, List<GameObject>>();

            foreach(Pool<T> pool in _pools)
            {
                List<GameObject> objectPool = new List<GameObject>();

                for(int i = 0; i < pool.ObjectsAmount; i++)
                {
                    GameObject pooledObject = Instantiate(pool.Prefab, gameObject.transform);
                    pooledObject.SetActive(false);

                    objectPool.Add(pooledObject);
                }

                _poolDictionary.Add(pool.Type, objectPool);
            }     
        }

        protected GameObject CreateObject(T type, Vector2 spawnPosition, Quaternion rotation)
        {
            GameObject someObject = _pools.Find(pool => pool.Type.Equals(type)).Prefab;

            GameObject objectToCreate = Instantiate(someObject, gameObject.transform);
            objectToCreate.gameObject.SetActive(true);
            objectToCreate.transform.position = spawnPosition;
            objectToCreate.transform.rotation = rotation;

            _poolDictionary[type].Add(objectToCreate);

            return objectToCreate;      
        }

        public GameObject GetObjectFromPool(T type, Vector2 spawnPosition, Quaternion rotation)
        {
            if(!_poolDictionary.ContainsKey(type))
            {
                Debug.LogWarning("This tag " + type + " doesn't exist.");
                return null;
            }

            for(int i = 0; i < _poolDictionary[type].Count; i++)
            {
                GameObject someObject = _poolDictionary[type][i];
                if(!someObject.activeInHierarchy)
                {
                    someObject.gameObject.SetActive(true);
                    someObject.transform.position = spawnPosition;
                    someObject.transform.rotation = rotation;

                    return someObject;
                }
            }

            if(_shouldExpand)
            {
                GameObject newObject = CreateObject(type, spawnPosition, rotation);

                return newObject;
            }

            return null;
        }

        public void ReturnObjectToPool(GameObject someObject, T type)
        {
            someObject.gameObject.SetActive(false);
        }
    }
}
