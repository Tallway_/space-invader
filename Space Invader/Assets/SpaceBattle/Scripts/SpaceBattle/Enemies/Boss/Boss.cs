﻿using System.Collections;
using System.Collections.Generic;
using SpaceBattle.Spawners;
using UnityEngine;

namespace SpaceBattle.Enemies.Boss
{
    [RequireComponent(typeof(Rigidbody2D), typeof(CircleCollider2D), typeof(Animator))]
    public class Boss : MonoBehaviour
    {
        [SerializeField] private EnemySpawner _enemySpawner;
        [Header("Boss stats"), SerializeField, Space(20)] private float _heath;
        [SerializeField] private float _scorePoints;
        [SerializeField, Range(0, 2)] private float _shootingRate;
        [SerializeField, Range(0, 1000)] private int _enemiesCountToSpawn;
        
    
        
        void Start()
        {
            
        }
    
        // Update is called once per frame
        void Update()
        {
            
        }
    }
}
