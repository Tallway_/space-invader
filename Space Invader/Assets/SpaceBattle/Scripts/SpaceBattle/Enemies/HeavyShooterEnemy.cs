﻿using System.Collections;
using SpaceBattle.Global;
using SpaceBattle.Interfaces;
using UnityEngine;

namespace SpaceBattle.Enemies
{
    public class HeavyShooterEnemy : Enemy, IActivatable, IDisactivatable, IDamagable
    {
        [SerializeField, Range(0, 4)] private float _speedPerSecond;

        private IEnumerator _movement;
        private Vector2 _positionToMove;
        private float _movementTime;

        private void Awake() 
        {
            ParentInit();

            _movement = Move();
            _shooting = Shoot();
        }

        private IEnumerator Move()
        {
            while(true)
            {
                _positionToMove.x = Random.Range(_movementBounds.MinX, _movementBounds.MaxX);
                _positionToMove.y = Random.Range(_movementBounds.MinY, _movementBounds.MaxY);

                yield return MoveToPoint(_positionToMove);
            }
        }

        private IEnumerator MoveToPoint(Vector2 endPosition)
        {
            float distance = Vector2.Distance(transform.position, _positionToMove);

            _movementTime = _speedPerSecond * distance; 

            Vector2 startPosition = transform.position;

            for(float i = 0; i < 1; i += Time.deltaTime / _movementTime)
            {
                transform.position = Vector2.Lerp(startPosition, endPosition, i);

                yield return null;
            }

            _movementTime = 0f;
        }

        protected override void OnActivate()
        {
            StartCoroutine(_movement);
            StartCoroutine(_shooting);
        }
    }
}
