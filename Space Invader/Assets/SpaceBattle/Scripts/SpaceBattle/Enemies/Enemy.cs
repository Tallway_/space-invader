﻿using System;
using System.Collections;
using System.Collections.Generic;
using SpaceBattle.Bullets;
using SpaceBattle.Enums;
using SpaceBattle.Global;
using SpaceBattle.ObjectPoolers;
using SpaceBattle.Ships;
using SpaceBattle.Spawners;
using SpaceBattle.UI;
using UnityEngine;

namespace SpaceBattle.Enemies
{
    [RequireComponent(typeof(BoxCollider2D))]
    public abstract class Enemy : MonoBehaviour
    {
        [SerializeField] protected EnemyType _enemyType;
        [SerializeField] protected BulletType _bulletType;
        [SerializeField, Space] protected EnemyStats _enemyStats;
        [SerializeField, Space] protected List<Transform> _bulletsSlots;
        [SerializeField, Space] protected float _shootingRate;
        [SerializeField, Space] protected ScoreDisplay _scoreDisplay;

        protected int _health;
        protected float _boostingValue;
        protected GameBounds _movementBounds;
        protected IEnumerator _shooting;
        protected Animator _animator;
        protected BoxCollider2D _boxCollider;
        protected Player _firstPlayer, _secondPlayer, _currentPlayer;
        protected List<Player> _players;

        public EnemyType EnemyType { get { return _enemyType; } }

        protected void ParentInit() 
        {
            _animator = GetComponent<Animator>();
            _boxCollider = GetComponent<BoxCollider2D>();

            _players = new List<Player>();

            Vector2 colliderHalfSize = (_boxCollider.size / 2) * transform.localScale.x;

            _movementBounds = new GameBounds(CameraInfo.CameraBounds.MinX + colliderHalfSize.x,
                                             CameraInfo.CameraBounds.MaxX - colliderHalfSize.x,
                                             0f + colliderHalfSize.y,
                                             CameraInfo.CameraBounds.MaxY - colliderHalfSize.y);


            _health = _enemyStats.ShipHealth;
            _boostingValue = 1f;

            GameObject.FindGameObjectWithTag("FirstPlayer")?.TryGetComponent<Player>(out _firstPlayer);
            GameObject.FindGameObjectWithTag("SecondPlayer")?.TryGetComponent<Player>(out _secondPlayer);

            TryAddPlayerToList(_firstPlayer);
            TryAddPlayerToList(_secondPlayer);
        }

        private void TryAddPlayerToList(Player player)
        {
            if(player != null)
            {
                _players.Add(player);
            }
        }

        protected void OnEnable() 
        {
            GameManager.Instance.OnBossKilled += OnBossKilled;
        }

        protected void OnDisable() 
        {
            GameManager.Instance.OnBossKilled -= OnBossKilled;
        }

        protected void OnBossKilled()
        {
            foreach (Player player in _players)
            {
                player.Score += _enemyStats.ScorePoints;
            }

            StopCoroutine(_shooting);

            StartCoroutine(PauseBeforeDestraction());        
        }

        private IEnumerator PauseBeforeDestraction()
        {
            float randomNumber = UnityEngine.Random.Range(0f, 1.5f);

            yield return new WaitForSeconds(randomNumber);

            DisplayEffects();
            ReturnToPool(); 
        }

        protected void OnTriggerEnter2D(Collider2D other) 
        {
            if(other.TryGetComponent(out Bullet bullet))
            {
                if(bullet.Owner == GameInstances.FirstPlayer)
                {
                    _currentPlayer = _firstPlayer;
                    ApplyDamage(bullet.DamageValue);
                }
                else if(bullet.Owner == GameInstances.SecondPlayer)
                {
                    _currentPlayer = _secondPlayer;
                    ApplyDamage(bullet.DamageValue);
                }

                bullet.Disactivate();
            }

            if(other.TryGetComponent(out ShipController ship))
            {
                DisplayEffects();

                ship.ApplyDamage(_enemyStats.CollisionDamage);

                Disactivate();
            }
        }

        protected IEnumerator Shoot()
        {
            WaitForSeconds pause = new WaitForSeconds(_shootingRate);

            while(true)
            {
                foreach(Transform slot in _bulletsSlots)
                {
                    GameObject bulletPrefab = BulletPooler.Instance.GetObjectFromPool(_bulletType, slot.position, slot.rotation);

                    Bullet bullet = bulletPrefab.GetComponent<Bullet>();
                    bullet.Init(GameInstances.Enemy, LayerMask.NameToLayer("EnemyBullet"));
                    bullet.Activate();
                }

                yield return pause;
            }
        }

        private void DisplayEffects()
        {
            ScoreDisplay scoreDisplay = Instantiate(_scoreDisplay, transform.position, Quaternion.identity);
            scoreDisplay.Init(_enemyStats.ScorePoints);

            AdditionalPooler.Instance.GetObjectFromPool(AdditionalType.Explosion, transform.position, Quaternion.identity);
        }

        public void ReturnToPool()
        {
            EnemyPooler.Instance.ReturnObjectToPool(this.gameObject, _enemyType);
        }

        public void ApplyDamage(int value)
        {
            _health -= value;

            if(_health <= 0)
            {
                _health = _enemyStats.ShipHealth;

                _currentPlayer.Score += _enemyStats.ScorePoints;

                BuffSpawner.Instance.TrySpawmBuff(transform);

                DisplayEffects();
                
                Disactivate();
            }
            else
            {
                _animator.SetTrigger("ApplyDamage");
            }
        }

        public void Disactivate()
        {
            StopAllCoroutines();
            ReturnToPool();
        }

        public void Activate()
        {
            OnActivate();
        }

        public void Boost(float boostingValue)
        {
            if(_boostingValue == boostingValue)
            {
                return;
            }
            else
            {
                _enemyStats.Boost(boostingValue);
            }
        }

        protected abstract void OnActivate();
    }
}
