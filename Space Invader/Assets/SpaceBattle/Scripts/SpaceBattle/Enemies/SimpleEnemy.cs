﻿using SpaceBattle.Interfaces;
using UnityEngine;

namespace SpaceBattle.Enemies
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class SimpleEnemy : Enemy, IActivatable, IDisactivatable, IDamagable
    {
        [SerializeField] private float _speed;

        private Rigidbody2D _rigidbody;

        private void Awake() 
        {
            _rigidbody = GetComponent<Rigidbody2D>();

            ParentInit();      
        }

        private void Move()
        {
            _rigidbody.velocity = Vector2.down * _speed;
        }

        protected override void OnActivate()
        {
            Move();
        }
    }
}
