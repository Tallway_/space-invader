﻿using System;
using UnityEngine;

namespace SpaceBattle.Enemies
{
    [Serializable]
    public class EnemyStats
    {
        public int ShipHealth;
        public int ScorePoints;
        public int CollisionDamage;

        private float _multiplier = 1f;

        public void Boost(float multiplier)
        {
            float coefficient = multiplier / _multiplier; 

            ShipHealth = Mathf.RoundToInt(ShipHealth * coefficient);
            ScorePoints = Mathf.RoundToInt(ScorePoints * coefficient);
            CollisionDamage = Mathf.RoundToInt(CollisionDamage * coefficient);

            _multiplier = multiplier;
        }
    }
}
