﻿using SpaceBattle.Global;
using SpaceBattle.Spawners;


namespace SpaceBattle.Wave.States
{
    public class WaveSpawningState : State<WaveManager>
    {
        private EnemySpawner _enemySpawner;

        public WaveSpawningState(WaveManager waveManager, StateMachine<WaveManager> stateMachine) : base(waveManager, stateMachine) 
        { 
            _enemySpawner = waveManager.EnemySpawner;
        }

        public override void Enter()
        { 
            _enemySpawner.Init();
            _enemySpawner.Activate();          
        }

        public override void Exit()
        {
            _manager.WaveCount++;
        }
    }
}
