﻿using System;
using SpaceBattle.Global;

namespace SpaceBattle.Wave.States
{
    public class GameEndingState : State<WaveManager>
    {
        public event Action OnWavesCompleted;

        public GameEndingState(WaveManager waveManager, StateMachine<WaveManager> stateMachine) : base(waveManager, stateMachine) 
        { }

        public override void Enter()
        {
            OnWavesCompleted?.Invoke();
        }
    }
}
