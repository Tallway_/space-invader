﻿using System;
using SpaceBattle.Global;

namespace SpaceBattle.Wave.States
{
    public class BossSpawningState : State<WaveManager>
    {
        public event Action OnBossDead;

        public BossSpawningState(WaveManager waveManager, StateMachine<WaveManager> stateMachine) : base(waveManager, stateMachine) { }

        public override void Enter()
        {
            
        }

        public override void Update()
        {
            
        }

        public override void Exit()
        {
            OnBossDead?.Invoke();
        }
    }
}
