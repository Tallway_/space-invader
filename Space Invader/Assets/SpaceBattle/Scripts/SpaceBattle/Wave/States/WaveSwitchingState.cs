﻿using System;
using SpaceBattle.Global;

namespace SpaceBattle.Wave.States
{
    public class WaveSwitchingState : State<WaveManager>
    {
        public event Action<int> OnWaveCountChanging;

        
        public WaveSwitchingState(WaveManager waveManager, StateMachine<WaveManager> stateMachine) : base(waveManager, stateMachine) { }

        public override void Enter()
        {
            OnWaveCountChanging?.Invoke(_manager.WaveCount + 1);

            if(_manager.WaveCount == _manager.Waves.Length)
            {
                //_waveManager.WaveMachine.ChangeState(_waveManager.BossSpawning);
                _manager.WaveMachine.ChangeState(_manager.GameEnding);
            }
            else
            {               
                _manager.WaveMachine.ChangeState(_manager.WaveSpawning);
            }
        }
    }   
}
