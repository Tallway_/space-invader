﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceBattle.Wave.ScriptableObjects
{
    [Serializable]
    public class EnemyTypePerWave
    {
        public GameObject Enemy;
        public float Probability;
    }
    
    [CreateAssetMenu(fileName = "WaveData", menuName = "Space Invader/WaveData", order = 0)]
    public class WaveData : ScriptableObject 
    {
        public List<EnemyTypePerWave> EnemiesPerWave;
        [Range (0, 3)] public float DifficultyMultiplier;
        [Range (0, 200)] public int EnemiesAmount; 
    }
}
