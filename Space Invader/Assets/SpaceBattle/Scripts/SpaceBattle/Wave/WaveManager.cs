﻿using System;
using System.Collections;
using System.Collections.Generic;
using SpaceBattle.Global;
using SpaceBattle.Spawners;
using SpaceBattle.Wave.ScriptableObjects;
using SpaceBattle.Wave.States;
using UnityEngine;

namespace SpaceBattle.Wave
{
    public class WaveManager : MonoBehaviour
    {
        #region Fields
        [SerializeField] 
        private WaveData[] _waves;
        
        [SerializeField, Space] 
        private EnemySpawner _enemySpawner;

        private int _waveCount;
        #endregion

        #region Properties
        public StateMachine<WaveManager> WaveMachine { get; private set; }
        public WaveSpawningState WaveSpawning { get; private set; }
        public WaveSwitchingState WaveSwitching { get; private set; }
        public BossSpawningState BossSpawning { get; private set; }
        public GameEndingState GameEnding { get; private set; }

        public EnemySpawner EnemySpawner { get { return _enemySpawner; } }
        public WaveData[] Waves { get { return _waves; } }

        public int WaveCount 
        { 
            get {  return _waveCount; } 
            set { _waveCount = value; } 
        }
        #endregion

        private void Awake() 
        {
            WaveCount = 0;

            WaveMachine = new StateMachine<WaveManager>();

            WaveSpawning = new WaveSpawningState(this, WaveMachine); 
            WaveSwitching = new WaveSwitchingState(this, WaveMachine);
            BossSpawning = new BossSpawningState(this, WaveMachine);
            GameEnding = new GameEndingState(this, WaveMachine);  
        }

        private void OnEnable() 
        {
            GameManager.Instance.OnGameStarted += OnGameStarted;
            GameManager.Instance.OnGameOver += OnGameOver;
        }

        private void OnDisable() 
        {
            GameManager.Instance.OnGameStarted += OnGameStarted;
            GameManager.Instance.OnGameOver -= OnGameOver;
        }

        private void OnGameOver()
        {
            _enemySpawner.Disactivate();
        }

        private void OnGameStarted()
        {
            WaveMachine.Initialize(WaveSwitching); 
            WaveMachine.CurrentState.Enter();
        }
    }
}
