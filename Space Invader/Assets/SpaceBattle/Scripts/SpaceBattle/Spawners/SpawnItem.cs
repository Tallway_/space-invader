﻿using System;
using UnityEngine;

namespace SpaceBattle.Spawners
{
    [Serializable]
    public class SpawnItem 
    {
        public float Probability;
        public GameObject Prefab;
    }
}