﻿using System.Collections;
using System.Collections.Generic;
using SpaceBattle.Asteroids;
using SpaceBattle.Enums;
using SpaceBattle.Global;
using SpaceBattle.Interfaces;
using SpaceBattle.ObjectPoolers;
using UnityEngine;

namespace SpaceBattle.Spawners
{
    public class AsteroidSpawner : MonoBehaviour, IActivatable, IDisactivatable
    {
        [SerializeField, Space] private float _spawnRate;
        [SerializeField, Space] private List<SpawnItem> _items;

        private GameBounds _spawnBounds;
        private IEnumerator _spawn;
        private Vector2 _positionToSpawn, _directionToPush;
        private float _sum;

        private void Awake() 
        {
            _spawnBounds = new GameBounds(CameraInfo.CameraBounds.MinX,
                                        CameraInfo.CameraBounds.MaxX,
                                        CameraInfo.CameraBounds.MaxY,
                                        CameraInfo.CameraBounds.MaxY + 1f);

            _spawn = SpawnAsteroid();

            _positionToSpawn = transform.position;
        }

        private void Start() 
        {
            foreach(SpawnItem item in _items)
            {
                _sum += item.Probability;
            }  
        }

        private void OnEnable() 
        {
            GameManager.Instance.OnGameStarted += OnGameStarted;
            GameManager.Instance.OnGameOver += OnGameOver;
        }

        private void OnDisable() 
        {
            GameManager.Instance.OnGameStarted -= OnGameStarted;
            GameManager.Instance.OnGameOver -= OnGameOver;
        }

        private void OnGameStarted()
        {
            Activate();
        }

        private void OnGameOver()
        {
            Disactivate();
        }

        private Asteroid GetAsteroid()
        {
            float currentSection = 0f;

            float randomNumber = Random.Range(0f, _sum);

            foreach(SpawnItem item in _items)
            {
                currentSection += item.Probability;

                if(randomNumber <= currentSection)
                {
                    return item.Prefab.GetComponent<Asteroid>();
                }
            }

            return null;
        }

        private IEnumerator SpawnAsteroid()
        {
            WaitForSeconds pause = new WaitForSeconds(_spawnRate);

            while(true)
            {
                _positionToSpawn.x = Random.Range(_spawnBounds.MinX, _spawnBounds.MaxX);
                _positionToSpawn.y = Random.Range(_spawnBounds.MinY, _spawnBounds.MaxY);

                _directionToPush.x = Random.Range(_spawnBounds.MinX, _spawnBounds.MaxX);
                _directionToPush.y = CameraInfo.CameraBounds.MinY - 10f;

                Asteroid _randomAsteroid = GetAsteroid();

                GameObject asteroidPrefab = AdditionalPooler.Instance.GetObjectFromPool(_randomAsteroid.Type, _positionToSpawn, transform.rotation);

                if(asteroidPrefab != null)
                {
                    Asteroid asteroid = asteroidPrefab.GetComponent<Asteroid>();
                    
                    asteroid.SetNormalizedMovementDirection(_directionToPush.normalized);
                    asteroid.Activate();

                    yield return pause;
                }
                else
                {              
                    yield return null;
                }
            }
        }

        public void Activate()
        {
            StartCoroutine(_spawn);
        }

        public void Disactivate()
        {
            StopAllCoroutines();
        }
    }
}
