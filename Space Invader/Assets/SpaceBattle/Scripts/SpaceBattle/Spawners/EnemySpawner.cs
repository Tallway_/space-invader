﻿using System.Collections;
using System.Collections.Generic;
using SpaceBattle.Enemies;
using SpaceBattle.Enums;
using SpaceBattle.Global;
using SpaceBattle.Interfaces;
using SpaceBattle.ObjectPoolers;
using SpaceBattle.Wave;
using SpaceBattle.Wave.ScriptableObjects;
using UnityEngine;

namespace SpaceBattle.Spawners
{
    public class EnemySpawner : MonoBehaviour, IActivatable, IDisactivatable
    {
        [SerializeField, Space] private float _spawnRate;
        [SerializeField, Space] private float _pauseBetweenWaves;
        [SerializeField, Space] private WaveManager _waveManager;

        private WaveData _waveData;
        private GameBounds _spawnBounds;
        private IEnumerator _spawn;
        private Vector2 _positionToSpawn;
        private float _sum;
        private int _enemiesCount;

        private void Awake() 
        {
            _spawnBounds = new GameBounds(CameraInfo.CameraBounds.MinX + 0.4f,
                                    CameraInfo.CameraBounds.MaxX - 0.4f,
                                    CameraInfo.CameraBounds.MaxY,
                                    CameraInfo.CameraBounds.MaxY + 1f);

            _spawn = SpawnEnemy();

            _positionToSpawn = transform.position;
        }

        public void Init() 
        {
            _waveData = _waveManager.Waves[_waveManager.WaveCount];
            _enemiesCount = _waveData.EnemiesAmount;
            _sum = 0;

            foreach(EnemyTypePerWave enemyData in _waveData.EnemiesPerWave)
            {
                _sum += enemyData.Probability;
            }       
        }

        public void Init(WaveData waveData) 
        {
            _waveData = waveData;
            _enemiesCount = _waveData.EnemiesAmount;
            _sum = 0;

            foreach(EnemyTypePerWave enemyData in _waveData.EnemiesPerWave)
            {
                _sum += enemyData.Probability;
            }       
        }

        private Enemy GetEnemy()
        {
            float currentSection = 0f;

            float randomNumber = Random.Range(0f, _sum);

            foreach(EnemyTypePerWave enemyData in _waveData.EnemiesPerWave)
            {
                currentSection += enemyData.Probability;

                if(randomNumber <= currentSection)
                {
                    Enemy returningEnemy = enemyData.Enemy.GetComponent<Enemy>();
                    return returningEnemy;
                }
            }

            return null;
        }

        private IEnumerator SpawnEnemy()
        {
            WaitForSeconds pause = new WaitForSeconds(_spawnRate);

            while(_enemiesCount > 0)
            {
                _enemiesCount--;

                _positionToSpawn.x = Random.Range(_spawnBounds.MinX, _spawnBounds.MaxX);
                _positionToSpawn.y = Random.Range(_spawnBounds.MinY, _spawnBounds.MaxY);

                Enemy enemy = GetEnemy();

                GameObject enemyPrefab = EnemyPooler.Instance.GetObjectFromPool(enemy.EnemyType, _positionToSpawn, transform.rotation);

                if(enemyPrefab != null)
                {
                    Enemy currentEnemy = enemyPrefab.GetComponent<Enemy>();
                    currentEnemy.Boost(_waveData.DifficultyMultiplier);
                    currentEnemy.Activate();

                    yield return pause;
                }
                else
                {              
                    yield return null;
                }
            }

            yield return new WaitForSeconds(_pauseBetweenWaves);

            _waveManager.WaveMachine.ChangeState(_waveManager.WaveSwitching);
        }

        public void Activate()
        {
            StartCoroutine(SpawnEnemy());
        }

        public void Disactivate()
        {
            StopAllCoroutines();
        }
    }
}
