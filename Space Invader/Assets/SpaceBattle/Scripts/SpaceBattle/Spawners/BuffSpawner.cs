﻿using System.Collections;
using System.Collections.Generic;
using SpaceBattle.Enums;
using SpaceBattle.Interfaces;
using SpaceBattle.ObjectPoolers;
using UnityEngine;

namespace SpaceBattle.Spawners
{
    public class BuffSpawner : MonoBehaviour
    {
        [SerializeField, Range(0, 100)] private int _chanceToSpawnItem;
        [Space, SerializeField] private List<SpawnItem> _items;
        
        private Transform _spawnPoint;

        public static BuffSpawner Instance { get { return _instance; } }
        private static BuffSpawner _instance;
        private float _sum;

        private void Awake() 
        {
            if(_instance != null && _instance != this)
            {
                Destroy(this.gameObject);
            }
            else
            {
                _instance = this;
            }
        }

        private void Start() 
        {
            foreach(SpawnItem item in _items)
            {
                _sum += item.Probability;
            }

            if(_chanceToSpawnItem != 0f)
            {
                _sum = _sum * 100 / _chanceToSpawnItem;
            }
        }

        private GameObject TryGetBuff()
        {
            float currentSection = 0f;

            float randomNumber = Random.Range(0f, _sum);

            foreach(SpawnItem item in _items)
            {
                currentSection += item.Probability;

                if(randomNumber <= currentSection)
                {
                    return item.Prefab;
                }
            }

            return null;
        }

        public void TrySpawmBuff(Transform point)
        {
            GameObject buffPrefab = TryGetBuff();

            if(buffPrefab != null)
            {
                IBuff buffInterface = buffPrefab.GetComponent<IBuff>();

                GameObject buffObject = BuffsPooler.Instance.GetObjectFromPool(buffInterface.BuffsType, point.position, Quaternion.identity);
                IActivatable buffActivator = buffObject.GetComponent<IActivatable>();

                buffActivator.Activate();
            }
        }
    }
}
