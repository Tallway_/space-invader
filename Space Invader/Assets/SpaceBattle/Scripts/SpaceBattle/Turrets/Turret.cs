﻿using System.Collections;
using SpaceBattle.Bullets;
using SpaceBattle.Enums;
using SpaceBattle.Ships;
using SpaceBattle.ObjectPoolers;
using UnityEngine;
using SpaceBattle.Global;

namespace SpaceBattle.Turrets
{
    public abstract class Turret : MonoBehaviour
    {
        [SerializeField] protected float _shootingRate;
        [SerializeField] protected TurretType _turretType;
        [SerializeField] protected BulletType _bulletType;

        protected SpriteRenderer _turretRenderer;
        protected IEnumerator _shooting;
        protected Player _player;
        protected GameInstances _playerType;

        public TurretType TurretType { get { return _turretType; } }

        
        protected void Init()
        {
            _turretRenderer = GetComponent<SpriteRenderer>();
            _player = GetComponentInParent<Player>();

            if(_player.gameObject.tag == "FirstPlayer")
            {
                _playerType = GameInstances.FirstPlayer;
            }
            else
            {
                _playerType = GameInstances.SecondPlayer;
            }

            _shooting = Shoot();
        }

        public void Activate()
        {
            _turretRenderer.enabled = true;
            StartCoroutine(_shooting);
        }

        public void Disactivate()
        {
            _turretRenderer.enabled = false;
            StopAllCoroutines();
        }

        protected IEnumerator Shoot()
        {
            WaitForSeconds pause = new WaitForSeconds(_shootingRate);

            while(true)
            {
                foreach(Transform slotTransform in TurretManager.Socket.ListOfSlots)
                {
                    GameObject bulletPrefab = BulletPooler.Instance.GetObjectFromPool(_bulletType, slotTransform.position, slotTransform.rotation);

                    Bullet bullet = bulletPrefab.GetComponent<Bullet>();
                    bullet.Init(_playerType, LayerMask.NameToLayer("PlayerBullet"));
                    bullet.Activate();
                }

                yield return pause;
            }
        }
    }
}
