﻿using SpaceBattle.Enums;
using SpaceBattle.Interfaces;
using SpaceBattle.ObjectPoolers;
using SpaceBattle.Ships;
using UnityEngine;

namespace SpaceBattle.Buffs
{
    [RequireComponent(typeof(Rigidbody2D), typeof(BoxCollider2D), typeof(SpriteRenderer))]
    public class BulletsBuff : MonoBehaviour, IBuff, IActivatable, IDisactivatable
    {
        [SerializeField] private BuffType _buffType;
        [SerializeField] private float _usingTime;
        [SerializeField, Range(0,5)] private float _speed;

        public BuffType BuffsType { get { return _buffType; } }
        public float UsingTime { get { return _usingTime; } }

        private Rigidbody2D _rigidbody;


        private void Awake() 
        {
            _rigidbody = GetComponent<Rigidbody2D>();
        }

        public void Activate()
        {
            _rigidbody.velocity = Vector2.down * _speed;
        }

        public void Disactivate()
        {
            BuffsPooler.Instance.ReturnObjectToPool(this.gameObject, _buffType);
        }

        private void OnTriggerEnter2D(Collider2D other) 
        {
            if(other.gameObject.TryGetComponent(out ShipController _ship))
            {
                _ship.ReceiveBuff(this);
            }       
        }
    }
}
