﻿using SpaceBattle.Bullets;
using SpaceBattle.Enums;
using SpaceBattle.Global;
using SpaceBattle.ObjectPoolers;
using SpaceBattle.Ships;
using SpaceBattle.Spawners;
using SpaceBattle.UI;
using UnityEngine;

namespace SpaceBattle.Asteroids
{
    [RequireComponent(typeof(CircleCollider2D), typeof(Rigidbody2D), typeof(Animator))]
    public class Asteroid : MonoBehaviour
    {
        protected readonly float _halfChance = 0.5f;

        [SerializeField] protected AdditionalType _type;
        [SerializeField] protected int _startHealthValue;
        [SerializeField] protected int _scorePoints;
        [SerializeField] protected float _rotationSpeed;
        [SerializeField] protected float _movementSpeed;
        [SerializeField] protected int _collisionDamage;
        [SerializeField, Space] protected ScoreDisplay _scoreDisplay;

        public AdditionalType Type
        { 
            get { return _type; }
        }

        protected CircleCollider2D _collider;
        protected Rigidbody2D _rigidbody;
        protected Animator _animator;
        protected int _rotationDirection;
        protected int _health;
        protected float _randomChance;
        protected Vector2 _normalizedMovementDirection;
        protected Player _firstPlayer, _secondPlayer, _currentPlayer;

        protected void ParentInit()
        {
            _collider = GetComponent<CircleCollider2D>();
            _rigidbody = GetComponent<Rigidbody2D>();
            _animator = GetComponent<Animator>();

            _firstPlayer = GameObject.FindGameObjectWithTag("FirstPlayer")?.GetComponent<Player>();
            _secondPlayer = GameObject.FindGameObjectWithTag("SecondPlayer")?.GetComponent<Player>();

            _health = _startHealthValue;
        }

        protected void OnEnable() 
        {
            _randomChance = Random.Range(0, 1); 
            _rotationDirection = _randomChance > _halfChance ? 1 : -1;

            GameUpdater.Instance.OnUpdate += OnUpdate;
        }

        protected void OnDisable() 
        {
            GameUpdater.Instance.OnUpdate -= OnUpdate;
        }

        private void OnUpdate()
        {
            Rotate();
        }

        protected void Rotate()
        {
            transform.Rotate(Vector3.forward * _rotationDirection * _rotationSpeed * Time.deltaTime);
        }

        protected void Move()
        {
            _rigidbody.velocity = _normalizedMovementDirection * _movementSpeed;
        }

        protected void ReturnToPool()
        {
            AdditionalPooler.Instance.ReturnObjectToPool(this.gameObject, _type);
        }

        public void Activate()
        {
            Move();
        }

        public void Disactivate()
        {
            ReturnToPool();
        }

        public virtual void ApplyDamage(int value)
        {
            _health -= value;

            if(_health <= 0)
            {
                _health = _startHealthValue;

                BuffSpawner.Instance.TrySpawmBuff(transform);
                _currentPlayer.Score += _scorePoints;

                DisplayEffects();

                ReturnToPool();
            }

            _animator.SetTrigger("ApplyingDamage");
        }

        public void SetNormalizedMovementDirection(Vector2 direction)
        {
            _normalizedMovementDirection = direction;
        }

        protected void OnTriggerEnter2D(Collider2D other) 
        {
            if(other.gameObject.TryGetComponent(out ShipController _ship))
            {
                DisplayEffects();

                _ship.ApplyDamage(_collisionDamage);
            } 

            if(other.TryGetComponent(out Bullet bullet))
            {
                if(bullet.Owner == GameInstances.FirstPlayer)
                {
                    _currentPlayer = _firstPlayer;
                    ApplyDamage(bullet.DamageValue);
                }
                else if(bullet.Owner == GameInstances.SecondPlayer)
                {
                    _currentPlayer = _secondPlayer;
                    ApplyDamage(bullet.DamageValue);
                }

                bullet.Disactivate();
            }      
        }

        protected void DisplayEffects()
        {
            ScoreDisplay scoreDisplay = Instantiate(_scoreDisplay, transform.position, Quaternion.identity);
            scoreDisplay.Init(_scorePoints);

            AdditionalPooler.Instance.GetObjectFromPool(AdditionalType.Explosion, transform.position, Quaternion.identity);
        }
    }
}
