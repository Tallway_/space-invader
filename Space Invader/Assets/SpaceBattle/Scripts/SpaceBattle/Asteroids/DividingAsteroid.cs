﻿using System.Collections;
using System.Collections.Generic;
using SpaceBattle.Global;
using SpaceBattle.Interfaces;
using SpaceBattle.ObjectPoolers;
using SpaceBattle.Spawners;
using UnityEngine;

namespace SpaceBattle.Asteroids
{
    public class DividingAsteroid : Asteroid, IActivatable, IDisactivatable, IDamagable 
    {
        [SerializeField, Space] private GameObject _simpleAsteroid;
        [SerializeField, Space] private List<Transform> _spawningSlots;

        private SimpleAsteroid _asteroid;

        private void Awake() 
        {
            ParentInit();

            _asteroid = _simpleAsteroid.GetComponent<SimpleAsteroid>();
        }

        public override void ApplyDamage(int value)
        {
            _health -= value;

            if(_health <= 0)
            {
                _health = _startHealthValue;

                BuffSpawner.Instance.TrySpawmBuff(transform);
                _currentPlayer.Score += _scorePoints;

                DisplayEffects();

                DivideAsteroid();

                ReturnToPool();
            }

            _animator.SetTrigger("ApplyingDamage");
        }

        private void DivideAsteroid()
        {
            Vector2 directionToPush;

            foreach(Transform slot in _spawningSlots)
            {
                directionToPush.x = Random.Range(CameraInfo.CameraBounds.MinX, CameraInfo.CameraBounds.MaxX);
                directionToPush.y = CameraInfo.CameraBounds.MinY - 10f;

                GameObject asteroidPrefab = AdditionalPooler.Instance.GetObjectFromPool(_asteroid.Type, slot.position, Quaternion.identity);
                SimpleAsteroid asteroid = asteroidPrefab.GetComponent<SimpleAsteroid>();

                asteroid.SetNormalizedMovementDirection(directionToPush.normalized);
                asteroid.Activate();
            }

            ReturnToPool();
        }
    }
}
