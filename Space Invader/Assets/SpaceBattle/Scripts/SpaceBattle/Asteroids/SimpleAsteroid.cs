﻿using System.Collections;
using System.Collections.Generic;
using SpaceBattle.Interfaces;
using UnityEngine;

namespace SpaceBattle.Asteroids
{
    public class SimpleAsteroid : Asteroid, IActivatable, IDisactivatable, IDamagable 
    {
        private void Awake() 
        {
            ParentInit();
        }
    }

}
