﻿using System;
using SpaceBattle.Wave;
using TMPro;
using UnityEngine;

namespace SpaceBattle.UI
{
    public class WaveInfo : MonoBehaviour
    {
        [SerializeField] private WaveManager _waveManager; 
        [SerializeField] private TextMeshProUGUI _waveText;
        [SerializeField] private TextMeshProUGUI _count;
        [SerializeField] private Animator _animator;

        private const string WaveChanged = nameof(WaveChanged);

        private void Start() 
        {
            _waveManager.WaveSwitching.OnWaveCountChanging += OnWaveCountChanging;
        }

        private void OnDisable() 
        {
            _waveManager.WaveSwitching.OnWaveCountChanging -= OnWaveCountChanging;
        }

        private void OnWaveCountChanging(int value)
        {
            if(value == _waveManager.Waves.Length + 1)
            {
                _waveText.text = "Final";
                _count.text = "Wave";
            }
            else
            {
                _count.text = value.ToString();
            }

            _animator.SetTrigger(WaveChanged);
        }
    }
}
