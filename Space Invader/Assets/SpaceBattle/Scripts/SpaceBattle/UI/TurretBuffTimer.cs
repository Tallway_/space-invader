﻿using System;
using System.Collections;
using System.Collections.Generic;
using SpaceBattle.Global;
using SpaceBattle.Ships;
using UnityEngine;
using UnityEngine.UI;

namespace SpaceBattle.UI
{
    public class TurretBuffTimer : MonoBehaviour
    {
        public static event Action OnTurretBuffTimerOver;

        [SerializeField] private Image _timerImage;
        [SerializeField] private Image _bulletImage;
        [SerializeField] private float _timerStep;

        private TurretManager _turretManager;
        private float _timer, _maxTimerValue;
        
        private void Awake() 
        {
            _bulletImage.enabled = false;
            _timerImage.enabled = false;

            _timer = 0f;
            _maxTimerValue = 0f;
        }

        private void Start()
        {
            _turretManager = FindObjectOfType<TurretManager>();

            GameManager.Instance.OnGameOver += OnGameOver;
            _turretManager.OnTimeAdding += OnTimeAdding;
            _turretManager.OnTurretChanged += OnTurretChanged;
        }

        private void OnDisable() 
        {
            GameManager.Instance.OnGameOver -= OnGameOver;
            _turretManager.OnTimeAdding -= OnTimeAdding;
            _turretManager.OnTurretChanged -= OnTurretChanged;       
        }

        private void OnGameOver()
        {
            StopAllCoroutines();
            _bulletImage.enabled = false;
            _timerImage.enabled = false;
            _timer = 0f;
            _maxTimerValue = 0f;
        }

        private void OnTurretChanged(Sprite turretSprite, float usingTime)
        {
            StopAllCoroutines();

            _bulletImage.enabled = true;
            _timerImage.enabled = true;
            _bulletImage.sprite = turretSprite;
            _timer = usingTime;
            _maxTimerValue = usingTime;

            StartCoroutine(LauchCountdown());
        }

        private void OnTimeAdding(float additionTime)
        {
            _timer += additionTime;
        }

        private IEnumerator LauchCountdown()
        {
            WaitForSeconds pause = new WaitForSeconds(_timerStep);

            while(_timer > 0f)
            {
                if(_timer > _maxTimerValue)
                {
                    _maxTimerValue = _timer;
                }

                _timerImage.fillAmount = Mathf.Clamp(_timer / _maxTimerValue, 0f, 1f);
                _timer -= _timerStep;

                yield return pause;
            }

            _timer = 0f;
            _maxTimerValue = 0f;
            _bulletImage.enabled = false;
            _timerImage.enabled = false;

            OnTurretBuffTimerOver?.Invoke();
        }
    }
}
