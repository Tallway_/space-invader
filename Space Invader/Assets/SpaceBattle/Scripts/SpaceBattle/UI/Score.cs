﻿using SpaceBattle.Global;
using TMPro;
using UnityEngine;

namespace SpaceBattle.UI
{
    public class Score : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _scoreText;
        [SerializeField] private bool _isWatchFirstPlayer;
        [SerializeField] private bool _isWatchSecondPlayer;

        private Player _player;

        private void Awake() 
        {
            if(_isWatchFirstPlayer)
            {
                _player = GameObject.FindGameObjectWithTag("FirstPlayer").GetComponent<Player>();
                _isWatchSecondPlayer = false;
            }

            if(_isWatchSecondPlayer)
            {
                _player = GameObject.FindGameObjectWithTag("SecondPlayer").GetComponent<Player>();
                _isWatchFirstPlayer = false;
            }
        }

        private void OnEnable() 
        {
            _player.OnScoreChanged += OnScoreChanged;
        }

        private void OnDisable() 
        {
            _player.OnScoreChanged -= OnScoreChanged;
        }

        private void OnScoreChanged(int changedValue)
        {
            _scoreText.text = changedValue.ToString();
        }
    }
}
