﻿using SpaceBattle.Global;
using SpaceBattle.Ships;
using TMPro;
using UnityEngine;

namespace SpaceBattle.UI
{
    public class Health : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _healthText;
        [SerializeField] private bool _isWatchFirstPlayer;
        [SerializeField] private bool _isWatchSecondPlayer;
        
        private Player _player;
        private ShipController _ship;
        
        private void Awake() 
        {
            if(_isWatchFirstPlayer)
            {
                _player = GameObject.FindGameObjectWithTag("FirstPlayer").GetComponent<Player>();
                _isWatchSecondPlayer = false;
            }

            if(_isWatchSecondPlayer)
            {
                _player = GameObject.FindGameObjectWithTag("SecondPlayer").GetComponent<Player>();
                _isWatchFirstPlayer = false;
            }

            _ship = _player.gameObject.GetComponentInChildren<ShipController>();
        }

        private void OnEnable() 
        {
            _ship.OnHealthChanged += OnHealthChanged;

            _healthText.text = _ship.Health.ToString();
        }

        private void OnDisable() 
        {
            _ship.OnHealthChanged -= OnHealthChanged;
        }

        private void OnHealthChanged(int changedValue)
        {
            _healthText.text = changedValue.ToString();
        }
    }
}
