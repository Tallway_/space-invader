﻿using System;
using System.Collections;
using System.Collections.Generic;
using SpaceBattle.Global;
using SpaceBattle.Ships;
using UnityEngine;
using UnityEngine.UI;

namespace SpaceBattle.UI
{
    public class BulletBuffTimer : MonoBehaviour
    {
        public static event Action OnBulletBuffTimerOver;

        [SerializeField] private Image _timerImage;
        [SerializeField] private Image _socketImage;
        [SerializeField] private float _timerStep;

        private SocketManager _socketManager;
        private float _timer, _maxTimerValue;
        
        private void Awake() 
        {
            _socketImage.enabled = false;
            _timerImage.enabled = false;

            _timer = 0f;
            _maxTimerValue = 0f;
        }

        private void Start()
        {
            _socketManager = FindObjectOfType<SocketManager>();

            GameManager.Instance.OnGameOver += OnGameOver;
            _socketManager.OnTimeAdding += OnTimeAdding;
            _socketManager.OnSocketChanged += OnSocketChanged;
        }

        private void OnDisable() 
        {
            GameManager.Instance.OnGameOver -= OnGameOver;
            _socketManager.OnTimeAdding -= OnTimeAdding;
            _socketManager.OnSocketChanged -= OnSocketChanged;       
        }

        private void OnGameOver()
        {
            StopAllCoroutines();

            _socketImage.enabled = false;
            _timerImage.enabled = false;
            _timer = 0f;
            _maxTimerValue = 0f;
        }

        private void OnSocketChanged(Socket socket, float usingTime)
        {
            StopAllCoroutines();

            _socketImage.enabled = true;
            _timerImage.enabled = true;
            _socketImage.sprite = socket.SocketSpite;
            _timer = usingTime;
            _maxTimerValue = usingTime;

            StartCoroutine(LauchCountdown());
        }

        private void OnTimeAdding(float additionTime)
        {
            _timer += additionTime;
        }

        private IEnumerator LauchCountdown()
        {
            WaitForSeconds pause = new WaitForSeconds(_timerStep);

            while(_timer > 0f)
            {
                if(_timer > _maxTimerValue)
                {
                    _maxTimerValue = _timer;
                }

                _timerImage.fillAmount = Mathf.Clamp(_timer / _maxTimerValue, 0f, 1f);
                _timer -= _timerStep;

                yield return pause;
            }

            _timer = 0f;
            _maxTimerValue = 0f;
            _socketImage.enabled = false;
            _timerImage.enabled = false;

            OnBulletBuffTimerOver?.Invoke();
        }
    }
}
