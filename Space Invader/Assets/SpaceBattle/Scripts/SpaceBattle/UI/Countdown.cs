﻿using SpaceBattle.Global;
using UnityEngine;
using TMPro;

namespace SpaceBattle.UI
{
    public class Countdown : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _countdown;
        [SerializeField] private Animator _animator;
    
        private const string CountdownStarted = nameof(CountdownStarted);
    
        private void OnEnable() 
        {
            GameManager.Instance.OnCountdownValueChanged += OnCountdownValueChanged;
            GameManager.Instance.OnGameStarted += OnGameStarted;
        }
    
        private void OnDisable() 
        {
            GameManager.Instance.OnCountdownValueChanged -= OnCountdownValueChanged;
            GameManager.Instance.OnGameStarted -= OnGameStarted;       
        }
    
        private void OnGameStarted()
        {
            gameObject.SetActive(false);
        }
    
        private void OnCountdownValueChanged(int value)
        {
            _countdown.text = value.ToString();
    
            _animator.SetTrigger(CountdownStarted);
        }
    }
}