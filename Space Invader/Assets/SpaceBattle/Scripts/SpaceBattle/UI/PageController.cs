﻿using SpaceBattle.Global;
using UnityEngine;

namespace SpaceBattle.UI
{
    public class PageController : MonoBehaviour
    {
        [SerializeField] private Animator _animator;

        private const string GameOver = nameof(GameOver);

        private void OnEnable() 
        {
            GameManager.Instance.OnGameOver += OnGameOver;
        }

        private void OnDisable() 
        {
            GameManager.Instance.OnGameOver -= OnGameOver;
        }

        private void OnGameOver()
        {
            _animator.SetTrigger(GameOver);
        }
    }
}
