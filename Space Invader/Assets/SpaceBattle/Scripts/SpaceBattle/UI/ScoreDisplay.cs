﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceBattle.UI
{
    public class ScoreDisplay : MonoBehaviour
    {
        [SerializeField] private TextMesh _score;

        private float _scoreValue;

        public void Init(float scoreValue)
        {
            _score.text = "+" + scoreValue.ToString();
        }

        public void Destroy()
        {
            Destroy(this.gameObject);
        } 
    }
}
