﻿namespace SpaceBattle.Enums
{
    public enum TurretType 
    {
        SimpleTurret,
        HeavyTurret,
        RapidfireTurret,
        RapidfireHeavyTurret
    }
}
