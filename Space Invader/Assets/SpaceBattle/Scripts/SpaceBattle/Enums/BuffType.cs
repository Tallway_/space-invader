﻿namespace SpaceBattle.Enums
{
    public enum BuffType
    {
        OneBulletShot,
        TwoBulletShot,
        OneBulletsAngleShot,
        TwoBulletsAngleShot,
        Rapidfire,
        Heavy,
        HeavyRapidfire,
        RepairTool,
        Shield
    }
}