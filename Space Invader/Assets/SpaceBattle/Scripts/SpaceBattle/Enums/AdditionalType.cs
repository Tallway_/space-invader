﻿namespace SpaceBattle.Enums
{
    public enum AdditionalType
    {
        SimpleAsteroid,
        DividingAsteroid,
        Wreackage,
        Explosion
    }
}
