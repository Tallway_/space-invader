﻿namespace SpaceBattle.Enums
{
    public enum BulletType
    {
        Simple,
        Fast,
        Heavy,
        FastHeavy
    }
}