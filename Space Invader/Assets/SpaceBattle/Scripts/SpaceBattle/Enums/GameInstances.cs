﻿namespace SpaceBattle.Enums
{
    public enum GameInstances 
    {
        FirstPlayer,
        Enemy,
        Buff,
        Obstacle,
        Other,
        SecondPlayer
    }
}
