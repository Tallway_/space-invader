﻿namespace SpaceBattle.Enums
{
    public enum EnemyType
    {
        Simple,
        Shooter,
        HeavyShooter
    }
}
