﻿using SpaceBattle.ObjectPoolers;
using UnityEngine;

namespace SpaceBattle.Effects
{
    public class ExplosionEffect : MonoBehaviour
    {
        private ParticleSystem _explosion;
    
        private void Awake() 
        {
            _explosion = GetComponent<ParticleSystem>();
        }
    
        private void OnDisable() 
        {
            AdditionalPooler.Instance.ReturnObjectToPool(this.gameObject, SpaceBattle.Enums.AdditionalType.Explosion);
        }
    }
}
